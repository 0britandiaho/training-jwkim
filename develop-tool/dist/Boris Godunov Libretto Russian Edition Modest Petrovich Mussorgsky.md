
 
# Boris Godunov: An Opera Masterpiece by Modest Mussorgsky
 
Boris Godunov is an opera by Modest Mussorgsky (1839-1881), one of the most influential Russian composers of the 19th century. The opera is based on the historical figure of Boris Godunov, who ruled as Tsar of Russia from 1598 to 1605 during a turbulent period known as the Time of Troubles. The opera explores the themes of power, guilt, ambition, and fate through the dramatic events of Boris's reign and his downfall by a pretender to the throne, the False Dmitriy.
 
The opera was composed between 1868 and 1873 in Saint Petersburg, Russia. Mussorgsky wrote his own libretto, drawing inspiration from the "dramatic chronicle" Boris Godunov by Aleksandr Pushkin, and from Nikolay Karamzin's History of the Russian State. The opera consists of four acts and a prologue, and features a large cast of characters, including boyars, monks, peasants, soldiers, and foreign guests. The music is rich in folk melodies, choruses, and orchestral effects, reflecting Mussorgsky's innovative and original style.
 
**Download ★★★★★ [https://urlcod.com/2uWLFB](https://urlcod.com/2uWLFB)**


 
The opera was first performed in its original version in 1874 at the Mariinsky Theatre in Saint Petersburg, but it was not well received by the critics or the public. Mussorgsky revised the opera in 1872, adding new scenes and characters, and changing some of the music. This version was premiered in 1896 in Moscow, after Mussorgsky's death, with some further alterations by his friend and colleague Nikolay Rimsky-Korsakov. Rimsky-Korsakov's version became the standard one for many years, until the original version was rediscovered and performed in the 20th century.
 
Boris Godunov is widely regarded as Mussorgsky's masterpiece and one of the greatest operas of all time. It has been performed and recorded by many famous singers, conductors, and orchestras around the world. It has also inspired other works of art, such as paintings, films, and novels. The opera is a powerful and moving portrayal of a tragic hero and a turbulent era in Russian history.
  
## Synopsis of Boris Godunov
 
The opera is divided into four parts, each consisting of one or two scenes. The following synopsis is based on the original 1869 version by Mussorgsky.
 
### Part 1 / Prologue
 
The opera begins with a prologue that sets the historical and political context of the story. In 1598, Tsar Fyodor has died without leaving an heir, and his brother-in-law Boris Godunov is the most likely candidate to succeed him. However, Boris is haunted by the rumor that he ordered the murder of Dmitry, Fyodor's younger brother and the last legitimate heir of the Rurik dynasty. The boyars (nobles) and the Orthodox Church pressure Boris to accept the crown, but he hesitates and pretends to be reluctant.
 
The prologue consists of two scenes: the first one takes place outside the Novodevichy Monastery near Moscow, where Boris has retired. A crowd of people gathers to beg him to become their tsar, but he refuses to come out. A police officer tries to force them to kneel and pray, but they resist and complain about their misery and oppression. A group of pilgrims arrives and sings a hymn, praising God and Russia. The second scene takes place in the Kremlin Square in Moscow, where Boris is finally crowned as tsar. He appears before the people and gives a speech, expressing his humility and devotion to God and Russia. He asks for their prayers and blessings, and promises to rule with justice and mercy. The people cheer and acclaim him, but Boris is still troubled by his conscience.
 
### Part 2 / Act 1
 
The first act takes place seven years later, in 1605. The scene is an inn on the Lithuanian border, where a group of vagabond monks are drinking and singing. One of them, Grigory Otrepiev, is a young runaway novice who has learned about the murder of Dmitry from an old monk, Pimen. Grigory decides to impersonate Dmitry and claim the throne, hoping to overthrow Boris and avenge his death. He escapes from the inn with the help of the innkeeper's daughter, who is in love with him. He is pursued by two agents of Boris, who suspect his identity.
 
Boris Godunov opera by Mussorgsky synopsis and analysis,  Mussorgsky's Boris Godunov Rimsky-Korsakov version libretto,  Boris Godunov based on Pushkin and Karamzin history of Russia,  Boris Godunov Russian edition with English translation and commentary,  Mussorgsky's masterpiece Boris Godunov characters and themes,  Boris Godunov Time of Troubles and False Dmitriy in Russian history,  Mussorgsky's Boris Godunov original and revised versions comparison,  Boris Godunov opera performance and recording reviews and recommendations,  Mussorgsky's Boris Godunov musical style and influences,  Boris Godunov opera plot summary and scene guide,  Mussorgsky's Boris Godunov historical accuracy and artistic license,  Boris Godunov opera adaptations and variations by other composers,  Mussorgsky's Boris Godunov cultural and political significance,  Boris Godunov opera best arias and choruses,  Mussorgsky's Boris Godunov orchestration and instrumentation,  Boris Godunov opera staging and production challenges and solutions,  Mussorgsky's Boris Godunov biographical and autobiographical elements,  Boris Godunov opera symbolism and imagery,  Mussorgsky's Boris Godunov critical reception and legacy,  Boris Godunov opera online streaming and download options,  Mussorgsky's Boris Godunov sources and references,  Boris Godunov opera trivia and fun facts,  Mussorgsky's Boris Godunov quotes and excerpts,  Boris Godunov opera tickets and venues near me,  Mussorgsky's Boris Godunov related works and composers,  Boris Godunov opera analysis of musical themes and motifs,  Mussorgsky's Boris Godunov libretto annotations and glossary,  Boris Godunov opera costumes and sets design and inspiration,  Mussorgsky's Boris Godunov influence on Russian literature and art,  Boris Godunov opera appreciation and education resources,  Mussorgsky's Boris Godunov personal and professional challenges during composition,  Boris Godunov opera role of chorus and crowd scenes,  Mussorgsky's Boris Godunov psychological portrait of the protagonist,  Boris Godunov opera comparison with other operas based on historical figures,  Mussorgsky's Boris Godunov use of folk songs and dances in the score,  Boris Godunov opera dramaturgy and structure of the scenes,  Mussorgsky's Boris Godunov exploration of power, guilt, and fate themes,  Boris Godunov opera famous singers and conductors who performed it,  Mussorgsky's Boris Godunov innovations and experiments in operatic form,  Boris Godunov opera study guide and quiz questions for students
 
### Part 3 / Act 2
 
The second act takes place in Moscow, in Boris's private chambers in the Kremlin. Boris is tormented by nightmares and hallucinations of the murdered Dmitry. He tries to find solace in his children, Fyodor and Xenia, but he is interrupted by a boyar, Shuisky, who brings him alarming news: a pretender has appeared in Poland, claiming to be Dmitry and gathering support from the Polish nobility and the Catholic Church. Boris is shocked and enraged by this news, and accuses Shuisky of lying and conspiring against him. He demands proof of Dmitry's death, and Shuisky tells him that he saw his body with his own eyes. Boris calms down a bit, but he is still suspicious and fearful. He dismisses Shuisky and prays for God's guidance.
 
### Part 4 / Act 4
 
The final act takes place in two scenes: the first one in a forest near Kromy, where the pretender Dmitry has invaded Russia with a Polish army and has been welcomed by many Russians as their true tsar. He is accompanied by Marina Mniszech, a Polish princess who loves him and hopes to become his queen. They encounter a group of peasants who have been mistreated by Boris's officials, and Dmitry promises them justice and freedom. He also meets a holy fool, who denounces him as an impostor and a traitor. Dmitry tries to win him over with money, but he refuses it.
 
The second scene takes place in the Faceted Palace in Moscow, where Boris is dying. He summons his son Fyodor and gives him his last advice: to be strong, wise, and pious; to love his people; and to beware of his enemies. He also asks Pimen to pray for his soul. Pimen arrives and tells him a miracle: he has seen the real Dmitry alive in a monastery near Uglich. Boris realizes that he has been innocent all along, and that God has punished him for nothing.
 63edc74c80
 
