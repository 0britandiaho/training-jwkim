
var tui = require('tui-code-snippet');
var CONST = require('./constant.js');
var TodoItem = require('./todoitem.js');
var DomHandler = require('./domhandler.js');

var Todo = tui.defineClass({

    /**
     * initialize
     * @param {Object} domElements - event target elements
     */
    init: function(domElements) {
        this.todoHashMap = new tui.HashMap();
        this.todoList = [];
        this.lastSeq = 0;
        this.filterType = CONST.FILTER_STATE.ALL;
        this.domHandler = new DomHandler(domElements);
        this._addEvent();
    },
    /**
     * add todo
     * @param {string} todoname - todo name
     */
    addTodo: function(todoname) {
        if (!todoname) {
            throw new Error('needTodoName');
        }
        this.lastSeq += 1;
        this.todoHashMap.set(this.lastSeq, new TodoItem(todoname, this.lastSeq));
        this.draw();
    },
    /**
     * delete uncompleted list
     */
    removeUncompleteTodo: function() {
        this.todoHashMap.each(tui.bind(function(obj, key) {
            if (obj.complete) {
                this.todoHashMap.remove(key);
            }
        }, this));
    },
    /**
     * todolist filterling
     * @returns {Array.<TodoItem>} - todolist array
     */
    updateArray: function() {
        this._makeTodoListArray();
        switch (this.filterType) {
            case CONST.FILTER_STATE.COMPLETE:
                this.todoList = this.completeArray;
                break;
            case CONST.FILTER_STATE.UNCOMPLETE:
                this.todoList = this.unCompleteArray;
                break;
            case CONST.FILTER_STATE.ALL:
            default:
                this.todoList = this.unCompleteArray.concat(this.completeArray);
                break;
        }

        return this.todoList;
    },
    /**
     * draw list at HTMLElement
     */
    draw: function() {
        this.updateArray();
        this.domHandler.printTodoListElement(this.todoList);
        this.domHandler.printInfoCountElement(this.unCompleteArray.length, this.completeArray.length);
        this.domHandler.printFilterElement(this.filterType);
    },
    /**
     * make todo array list
     */
    _makeTodoListArray: function() {
        var compareCreatedInDesc = function(prev, next) {
            return next.createDt - prev.createDt;
        };
        this.completeArray = this.todoHashMap.find(function(obj) {
            return obj.complete;
        });
        this.unCompleteArray = this.todoHashMap.find(function(obj) {
            return !obj.complete;
        });
        this.completeArray.sort(compareCreatedInDesc);
        this.unCompleteArray.sort(compareCreatedInDesc);
    },
    /**
     * addEventHandler
     */
    _addEvent: function() {
        this.domHandler.onInputKeyDownHandler(tui.bind(function(todoname) {
            this.addTodo(todoname);
        }, this));

        this.domHandler.onTodoCheckBoxClickHandler(tui.bind(function(seq) {
            this.todoHashMap.get(seq).toggleComplete();
            this.draw();
        }, this));

        this.domHandler.onRemoveButtonClickHandler(tui.bind(function() {
            this.removeUncompleteTodo();
            this.draw();
        }, this));

        this.domHandler.onFilterChangeButtonClickHandler(tui.bind(function(newFilterType) {
            if (this.filterType !== newFilterType) {
                this.filterType = newFilterType;
                this.draw();
            }
        }, this));
    }
});

module.exports = Todo;
